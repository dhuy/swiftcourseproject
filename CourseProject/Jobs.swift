//
//  Jobs.swift
//  CourseProject
//
//  Created by Dhuy on 9/5/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import Foundation
import UIKit

struct Jobs {
    let title: String?
    let description: String?
}
