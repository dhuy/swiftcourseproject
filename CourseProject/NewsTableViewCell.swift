//
//  NewsTableViewCell.swift
//  CourseProject
//
//  Created by Dhuy on 9/4/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellPicture: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellDescription: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.layer.cornerRadius = 2
        cellView.layer.masksToBounds = false
        cellView.layer.shadowOffset = CGSize(width: 1.5, height: 1)
        cellView.layer.shadowRadius = 2
        cellView.layer.shadowOpacity = 0.3
        
        cellLabel.adjustsFontSizeToFitWidth = false
        cellLabel.lineBreakMode = .byTruncatingTail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
