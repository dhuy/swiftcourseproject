//
//  HomeManager.swift
//  CourseProject
//
//  Created by Dhuy on 8/30/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import Foundation

final class HomeManager {
    
    public var homeSlides = [HomeSlide]()
    public var newsList = [News]()
    public var jobsList = [Jobs]()
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let shared = HomeManager()
    
    public func getJsonFromServer(_ callback: @escaping () -> Swift.Void) {
        let myUrl = URL(string: "https://bitbucket.org/dhuy/swiftcourseproject/raw/257c7ea1a7faddc87b8a9522c5c9936cc011d082/Project_Data.json");
        var request = URLRequest(url: myUrl!);
        
        request.httpMethod = "GET";
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { return }
            do {
                guard let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? NSDictionary
                    else {
                        return
                }
                
                let jsonSlides: NSArray = parsedData["HomeSlides"] as! NSArray
                let newsDict: NSArray = parsedData["News"] as! NSArray
                let jobsDict: NSArray = parsedData["Jobs"] as! NSArray
                
                for jsonSlide in jsonSlides {
                    let dict = jsonSlide as! NSDictionary
                    let slide = HomeSlide.init(
                        picture: dict["picture"] as? String,
                        title: dict["title"] as? String,
                        description: dict["description"] as? String)
                    
                    self.homeSlides.append(slide)
                }
                
                for newsItem in newsDict {
                    let dict = newsItem as! NSDictionary
                    let newsTemp = News.init(
                        picture: dict["picture"] as? String,
                        title: dict["title"] as? String,
                        description: dict["description"] as? String)
                    
                    self.newsList.append(newsTemp)
                }
                
                for jobItem in jobsDict {
                    let dict = jobItem as! NSDictionary
                    let jobsTemp = Jobs.init(
                        title: dict["title"] as? String,
                        description: dict["description"] as? String)
                    
                    self.jobsList.append(jobsTemp)
                }
                
                DispatchQueue.main.async() {
                    callback();
                }
            } catch let error as NSError {
                print(error)
            }
        }).resume()
        
    }
    
}
