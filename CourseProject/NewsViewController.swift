//
//  NewsViewController.swift
//  CourseProject
//
//  Created by Dhuy on 8/14/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit

class NewsViewController: UITableViewController {
    
    private var newsList = HomeManager.shared.newsList

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToThisViewController(segue: UIStoryboardSegue) {
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueToNewsDetail", sender: indexPath)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsTableViewCell
        cell.cellLabel.text = newsList[indexPath.row].title
        cell.cellDescription.text = newsList[indexPath.row].description
        cell.cellPicture?.sd_setImage(with: URL(string: newsList[indexPath.row].picture!))
        
        return cell
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? NewsDetailViewController {
            let indexPath = sender as! IndexPath
            vc.index = indexPath.row
        }
    }
}
