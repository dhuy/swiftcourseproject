//
//  Home.swift
//  CourseProject
//
//  Created by Dhuy on 8/30/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import Foundation
import UIKit

struct HomeSlide {
    let picture: String?
    let title: String?
    let description: String?
}
