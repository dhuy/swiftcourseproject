//
//  News.swift
//  CourseProject
//
//  Created by Dhuy on 9/5/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import Foundation
import UIKit

struct News {
    let picture: String?
    let title: String?
    let description: String?
}
