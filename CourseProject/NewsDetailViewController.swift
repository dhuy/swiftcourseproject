//
//  NewsDetailViewController.swift
//  CourseProject
//
//  Created by Dhuy on 9/5/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    private var newsList = HomeManager.shared.newsList
    
    public var index = 0
    
    @IBOutlet weak var newsDetailPicture: UIImageView!
    @IBOutlet weak var newsDetailTitle: UILabel!
    @IBOutlet weak var newsDetailDescription: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newsDetailPicture?.sd_setImage(with: URL(string: newsList[index].picture!))
        self.newsDetailTitle.text = newsList[index].title
        self.newsDetailDescription.text = newsList[index].description

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
