//
//  FirstPageViewController.swift
//  CourseProject
//
//  Created by Dhuy on 9/1/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit
import SDWebImage

class FirstPageViewController: UIViewController {

    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var firstTitle: UITextView!
    @IBOutlet weak var firstDescription: UITextView!
    
    private var homeSlides = HomeManager.shared.homeSlides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setImages()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setImages() {
        firstTitle.text = homeSlides[0].title
        firstDescription.text = homeSlides[0].description
        firstImage?.sd_setImage(with: URL(string: homeSlides[0].picture!))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
