//
//  MapViewController.swift
//  CourseProject
//
//  Created by Dhuy on 8/14/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let location = CLLocationCoordinate2DMake(-7.216343, -35.915126)
        
        let region = MKCoordinateRegionMakeWithDistance(location, 300, 300)
        map.setRegion(region, animated: false)
        
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = location
        dropPin.title = "Virtus"
        dropPin.subtitle = "UFCG"
        
        map.addAnnotation(dropPin)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
