//
//  SecondPageViewController.swift
//  CourseProject
//
//  Created by Dhuy on 9/1/17.
//  Copyright © 2017 Dhuy. All rights reserved.
//

import UIKit
import SDWebImage

class SecondPageViewController: UIViewController {

    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var secondTitle: UITextView!
    @IBOutlet weak var secondDescription: UITextView!
    
    private var homeSlides = HomeManager.shared.homeSlides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setImages()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setImages() {
        secondTitle.text = homeSlides[1].title
        secondDescription.text = homeSlides[1].description
        secondImage?.sd_setImage(with: URL(string: homeSlides[1].picture!))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
